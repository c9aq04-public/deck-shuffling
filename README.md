# Deck shuffling matters

[TOC]

## Motivation

There are several ways to shuffle a deck of cards:

1. The riffle shuffle, where the deck is separated into to halves, which are then merged together.
2. The Faro shuffle, which is a very special case of the riffle shuffle.
3. The overhand shuffle where the top cards of the deck are dealt a few at a times on the top of a (initially empty) pile.
4. The hindu shuffle, which produces a similar result to the overhand.
5. The pile shuffle, where cards are dealt on a numner of piles.
6. The Corgi shuffle, where the cards are spread and scrambled together.

When I began to play cards I used the overhand shuffle and sometimes a riffle shuffle. It was a disaster. I often got unlucky. Then I began to make piles and I got luckier.

Sometimes I did tournaments. It was compulsory to shuffle ones deck several times, using different techniques, before each game.

But why? And what makes a deck shuffled? Thats our main interest here.

Our purpose is:
1. To describe and study some **metrics** able to answer the question **« How shuffled is a deck of cards? »**.
2. To describe and study **how a person shuffles a deck**, in order to propose the best techniques a person can implement with their brain and hands.
3. To **compare**, using said metrics, the different **shuffling techniques**.

## What makes a good shuffle?

### Introduction

Imagine that the card are numbered: 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32.

The computer can easily shuffle them perfectly: in R the `sample(1:32)` yields for example 25, 20, 18, 17, 28, 12, 24, 31, 2, 19, 15, 9, 26, 10, 5, 14, 30, 7, 13, 27, 21, 8, 22, 6, 32, 3, 29, 1, 16, 4, 23, 11. It seems to be a good shuffle.

How comes? Naively, what I expect from a good shuffle is that **cards that are close of each other in the sorted deck aren't anymore in the shuffled deck**.

That's why I like pile shuffling, that's why I like the riffle shuffle when iterated three or four times in a row.

But!

Let's have a closer look at those two.

#### Overview: the riffle-shuffle

Given my 32 cards deck of cards, a crudely done riffle shuffle done just one time yields something like: 15, 16, 17, **1**, 18, 19, **2**, **3**, 20, **4**, **5**, 21, 22, **6**, 23, 24, **7**, **8**, 25, 26, **9**, 27, 28, **10**, 29, **11**, **12**, 30, 31, **13**, 32, **14**.

Even when dealing 6 cards to four different players after just the first shuffle, you get (I sorted the cards):
1. Player 1 gets 7, 9, 15, 18, 20, 22.
1. Player 2 gets 4, 6, 8, 16, 19, 27.
1. Player 3 gets 2, 5, 17, 23, 25, 28.
1. Player 4 gets 1, 3, 10, 21, 24, 26.

A little bit too deterministic if you ask me.

Doing another shuffle yields: _23_, _24_, **7**, **8**, 15, 16, _25_, 17, _26_, 1, **9**, 18, 19, _27_, _28_, 2, **10**, _29_, 3, 20, **11**, **12**, 4, 5, _30_, 21, 22, _31_, 6, **13**, _32_, **14**.
1. Player 1 gets 10, 11, 15, 19, 23, 26.
1. Player 2 gets 1, 12, 16, 24, 27, 29.
1. Player 3 gets 3, 4, 7, 9, 25, 28.
1. Player 4 gets 2, 5, 8, 17, 18, 20.

So okay, the suffle puts some space between cards, but the randomness notwithstanding, there still is structure there. The most obvious is that there are many monotonous subsequences. That must be a criteria: **monotonous sequences must be broken by the shuffle**.

#### Overview: the pile shuffle

If the cards are dealt one at a time, each on the next pile, up to the last one then again from the first one, then what one has achieved is nothing more than a perfect riffle-shuffle with _N_ hands (_N_ being, of course, the number of piles).

So of course the drawback is the same: structure.

### Choosing criteria for what makes a good shuffle

We've seen that monotonous subsequences are a plague. So are _glued_ packs of cards, cards that keep together after the shuffle.

That makes two objective criteria for a good deck shuffling:
1. The distance (i.e., the distribution of the distance) between two cards that were contiguous in the sorted deck must resemble that of a perfectly shuffled deck.
2. The correlation between the sorted sequence and the shuffled sequence must resemble that of a perfectly shuffled deck.

By **perfectly shuffled deck** we mean of course, **randomly, with automatic means**.


## How does a person shuffle a deck of cards?